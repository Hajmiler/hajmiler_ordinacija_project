﻿function provjera(){
	var email = document.getElementById("email").value;
	
	/*znak @*/
	var posOfAt = email.indexOf('@');
	
	if(posOfAt == -1){
		var p1 = 0;
	}else{
		var p1 = 1;
	}
	
	/*točka*/
	var posOfDot = email.indexOf('.', posOfAt);
	
	if(posOfDot == -1){
		var p2 = 0;
	}else{
		var p2 = 1;
	}
	
	/*dva znaka prije @*/
	var slc = email.slice(0, posOfAt);
	var len = slc.length;
	
	if(len < 2){
		var p3 = 0;
	}else{
		var p3 = 1;
	}
	
	/*dva znaka poslije @*/
	var slc2 = email.slice(posOfAt+1, posOfDot);
	var len2 = slc2.length;
	
	if(len2 < 2){
		var p4 = 0;
	}else{
		var p4 = 1;
	}
	
	/*dva znaka nakon točke*/
	var slc3 = email.slice(posOfDot+1);
	var len3 = slc3.length;
	
	if(len3 < 2){
		var p5 = 0;
	}else{
		var p5 = 1;
	}
	
	/*ako su svi jednaki jedan, znači da su ispunili sve uvjete te se vraća TRUE i upit će se poslati*/
	if(p1 == 1 && p2 == 1 && p3 == 1 && p4 == 1 && p5 == 1){
		return true;
	}else{
		alert("E-mail adresa MORA sadržavati:\n   Znak @,\n   Točku,\n   Min. dva znaka prije @,\n   Min. dva znaka nakon @,\n   Min. dva znaka nakon točke");
		return false;
	}
}
