<?php session_start(); ?> 
<html lang="hr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="family_icon.ico">
		<title>O nama</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="Oblikovanje.css">
		<link rel="stylesheet" type="text/css" href="OblikovanjeO_Nama.css">
    </head>
    <body>
        <div class="container-fluid" id="naslov">
            <div class="row">
				<div class="col-sm-12"><h1 align="center">Specijalistička ordinacija obiteljske medicine</h1><h3 align="center">Ivan Hajmiler, dr. med. spec. obiteljske medicine </h3></div>
				
			</div>
        </div>
		<br>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="index.php"><i class="fa fa-fw fa-home"></i>Naslovna</a>
                        </li>
                        <li class="active">
                            <a href="about us.php">O nama</a>
                        </li>
                        <?php
                            if (isset( $_SESSION['doctor_id'] ) ){
                                echo '<li> <a href="patients.php">Pacijenti</a> </li>';
								echo '<li> <a href="questions.php"><i class="fa fa-fw fa-envelope"></i>Pitanja</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
                            }
							if (isset( $_SESSION['patient_id'] ) ){
                                echo '<li> <a href="contact.php"><i class="fa fa-fw fa-envelope"></i>Kontakt</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
                            }
                        ?>
                    </ul>
					<ul class="nav navbar-nav navbar-right">
                        <?php
                            if (!isset( $_SESSION['doctor_id'] ) && !isset( $_SESSION['patient_id'] ) ){
                                echo '<li ><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>  Prijava</a> </li>';
                            }else{
                                echo '<li ><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>  Odjava</a>  </li>';
                            }
                        ?>
					</ul>
                </div>
            </nav>
            <div class="bg"><br><br>
				<div class="about_us">Ordinacija je osnovana 1991. godine u Našicama i među prvim je privatnim ordinacijama u Hrvatskoj. 
				Od samog početka osnovana je kao ordinacija Opće medicine s dijagnostikom, a tijekom godina razvila se u Specijalističku ordinaciju 
				Obiteljske medicine s dijagnostikom koja prvenstveno njeguje obiteljski i sveobuhvatni medicinski pristup svakom pojedincu koji se obrati 
				za pregled ili savjet. <br><br> U ordinaciji se prvenstveno njeguje razgovor s pacijentom u kojem liječnik doznaje osobitosti njegovih zdravstvenih tegoba, 
				ali i u kontekstu obiteljske situacije zbog eventualnih problema unutar obitelji, na poslu ili okolini pojedinca, jer sve su to mogući 
				uzroci tegoba ili nastale bolesti. <br><br> Njegujući takav pristup pacijentu, jedna tegoba koja je signal čovjeku zbog kojeg se javlja liječniku 
				može ovakvim sveobuhvatnim pristupom liječnika odvesti u sasvim drugom dijagnostičkom smjeru. To drugim riječima znači da se gušenje 
				u vratu u ovoj ordinaciji ne tretira samo kao simptom, već se traže uzroci i moguće posljedice. <br><br> Znači, u tom smislu bit će potreban 
				razgovor na početku susreta u ordinaciji, a tek zatim slijedi dijagnostika prema odluci liječnika i na kraju ukoliko se ne utvrdi organski 
				uzrok npr. štitnjača, srce, pluća ili nešto drugo, potrebno je ponovno tražiti mogući uzrok tegoba, ponovnim razgovorom s pacijentom 
				gdje se u razgovoru traže mogući uzroci tegoba. <br><br> Dakle ordinacija iako ima mogućnost dijagnostike ultrazvučnim pregledima najsuvremenijim 
				kolor doplerom Toshiba Aplio XG, EKG-om i drugim aparatima ne obavlja samo preglede npr. ultrazvuka trbušne šupljine već u ordinaciju 
				pacijenti dolaze s tegobama, a liječnik nakon razgovora i uvida u situaciju svakog pacijenta određuje što bi bilo potrebno od pregleda 
				učiniti i na kraju donosi dijagnozu bolesti ili stanja i odluku o daljnjem postupku liječenja. <br><br> Iz ordinacije pacijenti ne odlaze samo s nalazom,
				već sa objašnjenjem njihovog stanja i odgovorima na sva pacijentova pitanja uz objašnjenje uzroka tegoba i načinu liječenja do kojeg se 
				došlo razgovorom, kliničkim pregledom i dijagnostičkim pretragama u ordinaciji. <br><br> Vama na usluzi su Ivan Hajmiler, dr. med. spec. obiteljske
				medicine s dugogodišnjim iskustvom, kao i medicinska sestra Ana Anić.
				</div>
			</div>
        </div>
    </body>
    <footer>
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-1"></div>
                <div class="col-sm-4">
                    <div class="footer_font"><p>Specijalistička ordinacija obiteljske medicine</p></div>
                    <p>Ivan Hajmiler, dr. med. spec. obiteljske medicine </p>
                    <div class="footer_font"><p>Matije Gupca 106, Martin-Našice</p></div>
                </div>
                 <div class="col-sm-3">
                    <p>Radno vrijeme:</p>
                    <div class="footer_font"><p>Parni datumi: poslijepodne</p>
                    <p>Neparni datumi: prijepodne </p></div>
                </div>
                <div class="col-sm-3">
                    <p>Kontakt:</p>
                    <div class="footer_font"><p>Tel.: 031 / 695 - 219</p>
                    <p>E-mail: ivan.hajmiler@gmail.com</p></div>
                </div>
				<div class="col-sm-1"></div>
            </div>
        </div>
		<br>
    </footer>
</html>