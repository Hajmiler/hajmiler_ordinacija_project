<?php session_start();
include('connect.php');

$username="";
$email="";
$errors=array();

if (isset($_POST['reg_user'])) {
    $oib=mysqli_real_escape_string($conn, $_POST['oib']);
    $firstname=mysqli_real_escape_string($conn, $_POST['firstname']);
    $lastname=mysqli_real_escape_string($conn, $_POST['lastname']);
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password1 = mysqli_real_escape_string($conn, $_POST['password1']);
    $password2 = mysqli_real_escape_string($conn, $_POST['password2']);
  
    
    if (empty($oib)) { 
		array_push($errors, "OIB nedostaje"); 
	}
    if (empty($firstname)) { array_push($errors, "Ime nedostaje"); }
    if (empty($lastname)) { array_push($errors, "Prezime nedostaje"); }
    if (empty($username)) { array_push($errors, "Korisničko ime nedostaje"); }
    if (empty($email)) { array_push($errors, "Email nedostaje"); }
    if (empty($password1)) { array_push($errors, "Lozinka nedostaje"); }
    if (empty($password2)){array_push($errors, "Morate ponoviti lozinku");}
    if ($password1 != $password2) {
      array_push($errors, "Lozinke se ne podudaraju!");
    }
  
   
    $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
    $result = mysqli_query($conn, $user_check_query);
    $user = mysqli_fetch_assoc($result);
    
    if ($user) { 
      if ($user['username'] === $username) {
        array_push($errors, "Korisničko ime već postoji!");
      }
  
      if ($user['email'] === $email) {
        array_push($errors, "E-mail već postoji!");
      }
    }
  
    if (count($errors) == 0) {
        $password = md5($password1);
        $query=array();
        $query = "INSERT INTO users (oib, firstname, lastname, username, email, password) 
                  VALUES('$oib', '$firstname','$lastname','$username', '$email', '$password')";
        mysqli_query($conn, $query);
		array_push($errors, "Uspješno unesen pacijent!");
    }
}
?>
<html lang="hr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="family_icon.ico">
		<title>Registracija pacijenta</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="Oblikovanje.css">
		<link rel='stylesheet' type='text/css' href='OblikovanjeRegistracije.css'>
    <body>
        <div class="container-fluid" id="naslov">
            <div class="row">
				<div class="col-sm-12"><h1 align="center">Specijalistička ordinacija obiteljske medicine</h1><h3 align="center">Ivan Hajmiler, dr. med. spec. obiteljske medicine </h3></div>
				
			</div>
        </div>
		<br>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="index.php"><i class="fa fa-fw fa-home"></i>Naslovna</a>
                        </li>
                        <li>
                            <a href="about us.php">O nama</a>
                        </li>
                        <?php
                            if (isset( $_SESSION['doctor_id'] ) ){
                                echo '<li> <a href="patients.php">Pacijenti</a> </li>';
								echo '<li> <a href="questions.php"><i class="fa fa-fw fa-envelope"></i>Pitanja</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
								
                            }
                        ?>
                    </ul>
					<ul class="nav navbar-nav navbar-right">
                        <?php
                            if (!isset( $_SESSION['doctor_id'] ) ){
                                echo '<li ><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>  Prijava</a> </li>';
                            }else{
                                echo '<li ><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>  Odjava</a>  </li>';
                            }
                        ?>
					</ul>
                </div>
            </nav>
            <div class="bg"><br><br><br>
			<div class="registration">
				<div class="container">
					<form method="post" action="registration.php">
					<?php include('errors.php'); ?>
							<label for="oib"><b>OIB</b></label><br>
							<input type="text" placeholder="Unesi OIB" name="oib" required><br>
						
							<label for="firstname"><b>Ime</b></label><br>
							<input type="text" placeholder="Unesi ime" name="firstname" required><br>

							<label for="lastname"><b>Prezime</b></label><br>
							<input type="text" placeholder="Unesi prezime" name="lastname" required><br>

							<label for="username"><b>Korisničko ime</b></label><br>
							<input type="text" placeholder="Unesi korisničko ime" name="username" required><br>
			
							<label for="email"><b>E-mail</b></label><br>
							<input type="text" placeholder="Unesi e-mail" name="email" required><br>

							<label for="password1"><b>Lozinka</b></label><br>
							<input type="password" placeholder="Unesi lozinku" name="password1" required><br>

							<label for="password2"><b>Potvrda lozinke</b></label><br>
							<input type="password" placeholder="Ponovi unos lozinke" name="password2" required><br><br>
			
							<button type="submit" class="registerbtn" name="reg_user">Potvrdi unos</button>
						<br>
						<br>
					</form>
				</div> 
			</div>
			</div>
        </div>
    </body>
    <footer>
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-1"></div>
                <div class="col-sm-4">
                    <div class="footer_font"><p>Specijalistička ordinacija obiteljske medicine</p></div>
                    <p>Ivan Hajmiler, dr. med. spec. obiteljske medicine </p>
                    <div class="footer_font"><p>Matije Gupca 106, Martin-Našice</p></div>
                </div>
                 <div class="col-sm-3">
                    <p>Radno vrijeme:</p>
                    <div class="footer_font"><p>Parni datumi: poslijepodne</p>
                    <p>Neparni datumi: prijepodne </p></div>
                </div>
                <div class="col-sm-3">
                    <p>Kontakt:</p>
                    <div class="footer_font"><p>Tel.: 031 / 695 - 219</p>
                    <p>E-mail: ivan.hajmiler@gmail.com</p></div>
                </div>
				<div class="col-sm-1"></div>
            </div>
        </div>
		<br>
    </footer>
</html>
