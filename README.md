# Projekt "Ordinacija obiteljske medicine"

## (Izrada i korištenje web aplikacije "Ordinacija obiteljske medicine")

### Autor: Ivan Hajmiler

#### Izrada web aplikacije "Ordinacija obiteljske medicine"

##### Korišteni alati i tehnologije

- HTML-kratica od (eng.) HyperText Markup Language; prezentacijski jezik za izradu web stranica; definira sadržaj web stranice
- CSS-kratica od (eng.) Cascading Style Sheets; stilski jezik, koji se rabi za opis prezentacije dokumenta napisanog pomoću HTML-a
- JavaScript-skriptni programski jezik, koji se izvršava u web pregledniku na strani korisnika
- Bootstrap (front-end framework)-CSS radno okruženje (eng. framework); responzivni dizajn
- PHP-poslužiteljski skriptni jezik (eng. server scripting language)
- XAMPP-web poslužitelj
- MySQL-baza podataka
- Visual Studio Code-source-code editor

##### Uputstva za korištenje web aplikacije "Ordinacija obiteljske medicine"

Web aplikacija se može naći na adresi: https://hajac.000webhostapp.com. 

Posjetom danom linku otvara se naslovna stranica te se nude opcije: "Naslovna", "O Nama" i "Prijava". 

Klikom na opciju "Naslovna" osvježavamo ili se vraćamo na naslovnu stranicu. Klikom na opciju "O nama" otvaraju se osnovne informacije o ordinaciji. 

Klikom na opciju "Prijava" otvara se prozor koji traži unos odgovarajućeg korisničkog imena i lozinke te dugme za prijavu "Prijavi se". Moguće je prijaviti se kao doktor (administrator) ili pacijent pri čemu ovisno o prijavi nude se različite opcije i mogućnosti korištenja:

+ Kada se doktor obiteljske medicine prijavi (moguće je prijaviti se kao doktor s ovim podatcima -> KORISNIČKO IME: ihajmiler LOZINKA: ihajmiler) nude mu se četiri dodatne opcije: "Pacijenti", "Pitanja", "Q&A" te "Odjava". Odabirom opcije "Pacijenti" nude mu se četiri mogućnosti odabira ovisno o tome što želi raditi: "Pretraga pacijenata", "Brisanje pacijenata", "Izmjena pacijenata" i "Registracija pacijenata". Odabirom opcije "Pitanja" liječnik ima uvid u pitanja koja su mu postavili pacijenti te na ista može odgovoriti. Odabirom opcije "Q&A" liječnik vidi pitanja na koja je odgovorio. Odabirom opcije "Odjava" odjavljuje se sa svog profila i vraća na naslovnu stranicu.

+ Kada se pacijenti prijave (moguće je prijaviti se s ovim podatcima -> KORISNIČKO IME: bbarić LOZINKA: bbarić) nude im se dvije dodatne opcije: "Kontakt" i "Q&A". Odabirom opcije kontakt pacijent ima mogućnost poslati upit svom liječniku obiteljske medicine. Kada odabere opciju "Q&A" vidi odgovorena pitanja na upite koje je poslao svojem liječniku obiteljske medicine. Odabirom opcije "Odjava" odjavljuje se sa svog profila te se vraća na naslovnu stranicu.
