<?php
session_start();

	require "connect.php";	

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['submit'] == "Odgovori") {

			$rb = $_POST['id'];
			$odgovor = $_POST['odgovor'];
						
			$sql = "SELECT * FROM contact";
			$result = $conn->query($sql);
						
			$sql = "UPDATE contact SET answer = '$odgovor' WHERE id = $rb";
			$conn->query($sql);
			
			header("location: q&a.php");
		}
	}
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="family_icon.ico">
		<title>Odgovor na pitanje</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="OblikovanjeKontakta.css">
        <link rel="stylesheet" type="text/css" href="Oblikovanje.css">    
    </head>
    <body>
        <div class="container-fluid" id="naslov">
            <div class="row">
				<div class="col-sm-12"><h1 align="center">Specijalistička ordinacija obiteljske medicine</h1><h3 align="center">Ivan Hajmiler, dr. med. spec. obiteljske medicine </h3></div>
				
			</div>
        </div>
		<br>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-home"></i>Naslovna</a>
                        </li>
                        <li>
                            <a href="about us.php">O nama</a>
                        </li>
                        <?php
                            if (isset( $_SESSION['doctor_id'] ) ){
                                echo '<li> <a href="patients.php">Pacijenti</a> </li>';
								echo '<li> <a href="questions.php"><i class="fa fa-fw fa-envelope"></i>Pitanja</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
                            }
							if (isset( $_SESSION['patient_id'] ) ){
                                echo '<li> <a href="contact.php"><i class="fa fa-fw fa-envelope"></i>Kontakt</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
                            }
                        ?>
                    </ul>
					<ul class="nav navbar-nav navbar-right">
                        <?php
                            if (!isset( $_SESSION['doctor_id'] ) && !isset( $_SESSION['patient_id'] ) ){
                                echo '<li ><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>  Prijava</a> </li>';
                            }else{
                                echo '<li ><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>  Odjava</a>  </li>';
                            }
                        ?>
					</ul>
                </div>
            </nav>
            <div class="bg"><br><br><br>
			<div class="container">
				<div class="kontakt">
					<form action="doctors answer.php" method="post">
					
						<input type="hidden" name="id" value="<?=$_GET["id"]?>">
						<br>
						<label for="odgovor"><b>Odgovor</b></label><br>
						<input type="text" placeholder="Unesite odgovor" name="odgovor" id="odgovor" required>
						<br><br>
						<input id="answer1" type="submit" name="submit" class="btn" value="Odgovori" onClick="return provjera()";>
						
					</form>
				</div>
			</div>
        </div>
    </body>
    <footer>
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-1"></div>
                <div class="col-sm-4">
                    <div class="footer_font"><p>Specijalistička ordinacija obiteljske medicine</p></div>
                    <p>Ivan Hajmiler, dr. med. spec. obiteljske medicine </p>
                    <div class="footer_font"><p>Matije Gupca 106, Martin-Našice</p></div>
                </div>
                 <div class="col-sm-3">
                    <p>Radno vrijeme:</p>
                    <div class="footer_font"><p>Parni datumi: poslijepodne</p>
                    <p>Neparni datumi: prijepodne </p></div>
                </div>
                <div class="col-sm-3">
                    <p>Kontakt:</p>
                    <div class="footer_font"><p>Tel.: 031 / 695 - 219</p>
                    <p>E-mail: ivan.hajmiler@gmail.com</p></div>
                </div>
				<div class="col-sm-1"></div>
            </div>
        </div>
		<br>
    </footer>
</html>