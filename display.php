<?php session_start();
include('connect.php')?>

<html lang="hr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="family_icon.ico">
		<title>Prikaz pacijenata</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="Oblikovanje.css">
		<link rel='stylesheet' type='text/css' href='oblikovanjeUpdate.css'>
		<style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
        }
        th {
            text-align: left;
        }
		
        </style>
    </head>
    <body>
        <div class="container-fluid" id="naslov">
            <div class="row">
				<div class="col-sm-12"><h1 align="center">Specijalistička ordinacija obiteljske medicine</h1><h3 align="center">Ivan Hajmiler, dr. med. spec. obiteljske medicine </h3></div>
				
			</div>
        </div>
		<br>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="index.php"><i class="fa fa-fw fa-home"></i>Naslovna</a>
                        </li>
                        <li>
                            <a href="about us.php">O nama</a>
                        </li>
                        <?php
                            if (isset( $_SESSION['doctor_id'] ) ){
                                echo '<li> <a href="patients.php">Pacijenti</a> </li>';
								echo '<li> <a href="questions.php"><i class="fa fa-fw fa-envelope"></i>Pitanja</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
								
                            }
                        ?>
                    </ul>
					<ul class="nav navbar-nav navbar-right">
                        <?php
                            if (!isset( $_SESSION['doctor_id'] ) ){
                                echo '<li ><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>  Prijava</a> </li>';
                            }else{
                                echo '<li ><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>  Odjava</a>  </li>';
                            }
                        ?>
					</ul>
                </div>
            </nav>
            <div class="bg"><br><br><br>
			<div class="search">
				<?php 
				include('search.php');
				include('read.php');

					if ($count!=0){
						echo "<table border='1'style='width:100%' >
						<tr>
						<th>OIB</th>
						<th>Ime</th>
						<th>Prezime</th>
						<th>Korisničko ime</th>
						<th>Email</th>
						</tr>";
						
						while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
						{
						echo "<tr>";
						echo "<td>" . $row['oib'] . "</td>";
						echo "<td>" . $row['firstname'] . "</td>";
						echo "<td>" . $row['lastname'] . "</td>";
						echo "<td>" . $row['username'] . "</td>";
						echo "<td>" . $row['email'] . "</td>";
						echo "</tr>";
						}
					}elseif (isset($_POST['search']) || isset($_POST['all'])){
						echo "Nema rezultata za prikaz u bazi";
					}else{
						echo '';
					}
					echo "</table>";
				?>
				<br>
			</div>
			</div>
        </div>
    </body>
    <footer>
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-1"></div>
                <div class="col-sm-4">
                    <div class="footer_font"><p>Specijalistička ordinacija obiteljske medicine</p></div>
                    <p>Ivan Hajmiler, dr. med. spec. obiteljske medicine </p>
                    <div class="footer_font"><p>Matije Gupca 106, Martin-Našice</p></div>
                </div>
                 <div class="col-sm-3">
                    <p>Radno vrijeme:</p>
                    <div class="footer_font"><p>Parni datumi: poslijepodne</p>
                    <p>Neparni datumi: prijepodne </p></div>
                </div>
                <div class="col-sm-3">
                    <p>Kontakt:</p>
                    <div class="footer_font"><p>Tel.: 031 / 695 - 219</p>
                    <p>E-mail: ivan.hajmiler@gmail.com</p></div>
                </div>
				<div class="col-sm-1"></div>
            </div>
        </div>
		<br>
    </footer>
</html>

    