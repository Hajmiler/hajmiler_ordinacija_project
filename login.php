<?php session_start(); 
include('connect.php'); 

$unameErr=$pswErr="";
$uname=$psw="";
$errors=array();

if ($_SERVER["REQUEST_METHOD"]=="POST"){
	if (empty($_POST["uname"])){
		$unameErr="Potrebno je unijeti korisničko ime";
	}else{
		$uname=test_input($_POST["uname"]);
	}
			
	if (empty($_POST["psw"])){
		$pswErr="Potrebno je unijeti lozinku";
	}else{
		$psw=test_input($_POST["psw"]);
	}
}
	
function test_input($data){
	$data=trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

if(isset($_POST['submit'])) :
	$psw=md5($psw);
	$myusername = mysqli_real_escape_string($conn,$uname);
	$mypassword = mysqli_real_escape_string($conn,$psw);

	$sql = "SELECT oib, username FROM users WHERE username = '$myusername' and password = '$mypassword'";
	$result = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

	$count = mysqli_num_rows($result);
	  
	if($count == 1 and $row['username']=='ihajmiler') {
		$_SESSION['doctor_id']=$row['oib'];
		header("location: index.php");
	}elseif($count == 1 and $row['username']!='ihajmiler'){
		$_SESSION['patient_id']=$row['oib'];
		header("location: index.php");
	}else {
		array_push($errors, "Korisničko ime i/ili lozinka nisu ispravni!");
	}
endif;
?>

<html lang="hr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="family_icon.ico">
		<title>Specijalistička ordinacija obiteljske medicine Ivan Hajmiler, dr. med. spec. obiteljske medicine</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="Oblikovanje.css">
		<link rel="stylesheet" type="text/css" href="oblikovanjePrijave.css">
		<script type="text/javascript" src="functions.js"></script>
    </head>

    <body>
        <div class="container-fluid" id="naslov">
            <div class="row">
				<div class="col-sm-12"><h1 align="center">Specijalistička ordinacija obiteljske medicine</h1><h3 align="center">Ivan Hajmiler, dr. med. spec. obiteljske medicine </h3></div>
				
			</div>
        </div>
		<br>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="index.php"><i class="fa fa-fw fa-home"></i>Naslovna</a>
                        </li>
                        <li>
                            <a href="about us.php">O nama</a>
                        </li>
                    </ul>
					<ul class="nav navbar-nav navbar-right">
                        <?php
                            if (!isset( $_SESSION['doctor_id'] ) ){
                                echo '<li class="active"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>  Prijava</a> </li>';
                            }
                        ?>
					</ul>
                </div>
            </nav>
            <div class="bg"><br><br><br><br>
				<div class="container">
					<div class="login">
						<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
							<?php include('errors.php'); ?>
							<label for="uname"><b>Korisničko ime</b></label><span class="error">*<?php echo $unameErr; ?></span>
							<input type="text" placeholder="Unesi korisničko ime" name="uname" required >
							
							<br><br>

							<label for="psw" id="password"><b>Lozinka</b></label><span class="error">*<?php echo $pswErr; ?></span>
							<input type="password" placeholder="Unesi lozinku" name="psw" required >
							
							<br><br>

							<input type="submit" name="submit" value="Prijavi se">
						</form>
					</div>
				</div>
			</div>
        </div>
    <footer>
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-1"></div>
                <div class="col-sm-4">
                    <div class="footer_font"><p>Specijalistička ordinacija obiteljske medicine</p></div>
                    <p>Ivan Hajmiler, dr. med. spec. obiteljske medicine </p>
                    <div class="footer_font"><p>Matije Gupca 106, Martin-Našice</p></div>
                </div>
                 <div class="col-sm-3">
                    <p>Radno vrijeme:</p>
                    <div class="footer_font"><p>Parni datumi: poslijepodne</p>
                    <p>Neparni datumi: prijepodne </p></div>
                </div>
                <div class="col-sm-3">
                    <p>Kontakt:</p>
                    <div class="footer_font"><p>Tel.: 031 / 695 - 219</p>
                    <p>E-mail: ivan.hajmiler@gmail.com</p></div>
                </div>
				<div class="col-sm-1"></div>
            </div>
        </div>
		<br>
    </footer>
	</body>

</html>