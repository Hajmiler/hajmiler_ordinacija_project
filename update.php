<?php session_start();
include("connect.php"); ?>

<html lang="hr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="family_icon.ico">
		<title>Izmjena pacijenta</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
		<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="Oblikovanje.css">
		<link rel='stylesheet' type='text/css' href='oblikovanjeUpdate.css'>
    <body>
        <div class="container-fluid" id="naslov">
            <div class="row">
				<div class="col-sm-12"><h1 align="center">Specijalistička ordinacija obiteljske medicine</h1><h3 align="center">Ivan Hajmiler, dr. med. spec. obiteljske medicine </h3></div>
				
			</div>
        </div>
		<br>
        <div class="container-fluid">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="index.php"><i class="fa fa-fw fa-home"></i>Naslovna</a>
                        </li>
                        <li>
                            <a href="about us.php">O nama</a>
                        </li>
                        <?php
                            if (isset( $_SESSION['doctor_id'] ) ){
                                echo '<li> <a href="patients.php">Pacijenti</a> </li>';
								echo '<li> <a href="questions.php"><i class="fa fa-fw fa-envelope"></i>Pitanja</a> </li>';
								echo '<li> <a href="q&a.php">Q & A</a> </li>';
                            }
                        ?>
                    </ul>
					<ul class="nav navbar-nav navbar-right">
                        <?php
                            if (!isset( $_SESSION['doctor_id'] ) ){
                                echo '<li ><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>  Prijava</a> </li>';
                            }else{
                                echo '<li ><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>  Odjava</a>  </li>';
                            }
                        ?>
					</ul>
                </div>
            </nav>
            <div class="bg"><br><br><br>
			<div class="search">
			<?php include("search.php") ?>
			
            <?php
if ($count==1 && isset($_POST['search'])){
    while ($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $oib=$row['oib'];
        $firstname=$row['firstname'];
        $lastname=$row['lastname'];
        $username=$row['username'];
        $email=$row['email'];
    }
}elseif ($count==0 And isset($_POST['search'])){
    echo "Nema rezultata pretrage";
}else{
    echo "";
}

if (isset($_POST['change'])){
    $query="SELECT * FROM users WHERE firstname LIKE '%$fname%' AND lastname LIKE '%$lname%'";
    $result=mysqli_query($conn, $query);
    while ($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $oib=$row['oib'];
        $firstname=$row['firstname'];
        $lastname=$row['lastname'];
        $username=$row['username'];
        $email=$row['email'];
    }

    $query="UPDATE users SET oib='$_POST[oib]', firstname='$_POST[firstname]', lastname='$_POST[lastname]', username='$_POST[username]', email='$_POST[email]' WHERE firstname='$firstname'  AND lastname='$lastname' ";
    $result=mysqli_query($conn,$query);
}
?>
			
				<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
						<label for="oib"><b>OIB</b></label>
							<input type="text" placeholder="OIB" name="oib" value="<?php echo $oib; ?>" >
						
							<label for="firstname"><b>Ime</b></label>
							<input type="text" placeholder="Ime" name="firstname"  value="<?php echo $firstname; ?>">

							<label for="lastname"><b>Prezime</b></label>
							<input type="text" placeholder="Prezime" name="lastname" value="<?php echo $lastname; ?>" >

							<label for="username"><b>Korisničko ime</b></label>
							<input type="text" placeholder="Korisničko ime" name="username" value="<?php echo $username; ?>" >
			
							<label for="email"><b>E-mail</b></label>
							<input type="text" placeholder="E-mail" name="email" value="<?php echo $email; ?>" >

							<button type="submit" name="change" value="Submit" >Promijeni</button>
				</form>
			</div>
			</div>
        </div>
    </body>
    <footer>
        <div class="container-fluid">
            <div class="row">
				<div class="col-sm-1"></div>
                <div class="col-sm-4">
                    <div class="footer_font"><p>Specijalistička ordinacija obiteljske medicine</p></div>
                    <p>Ivan Hajmiler, dr. med. spec. obiteljske medicine </p>
                    <div class="footer_font"><p>Matije Gupca 106, Martin-Našice</p></div>
                </div>
                 <div class="col-sm-3">
                    <p>Radno vrijeme:</p>
                    <div class="footer_font"><p>Parni datumi: poslijepodne</p>
                    <p>Neparni datumi: prijepodne </p></div>
                </div>
                <div class="col-sm-3">
                    <p>Kontakt:</p>
                    <div class="footer_font"><p>Tel.: 031 / 695 - 219</p>
                    <p>E-mail: ivan.hajmiler@gmail.com</p></div>
                </div>
				<div class="col-sm-1"></div>
            </div>
        </div>
		<br>
    </footer>
	

</html>
